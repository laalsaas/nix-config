#! /usr/bin/env nix-shell
#! nix-shell -i bash -p rsync

# shellcheck shell=bash

set -eou pipefail

function show_help() {
  echo "Usage:"
  echo "$0 [--build-local] <host.hq.c3d2.de>"
  echo "--help          Show this help."
  echo "--build-local   Build config locally and copy it to the target system via nix copy"
  exit 1
}

if [[ $# == 0 ]]; then
  show_help
fi

while [[ $# -gt 0 ]]; do
  case "${1:-}" in
    "" | "-h" | "--help")
      show_help
      ;;
    "--build-local")
      build_local=true
      ;;
    *)
      host=$1
      ;;
  esac
  shift
done

if [[ -v build_local ]]; then
  hostname=$(ssh root@"$host" hostname)
  echo "$hostname> nix build"
  nix --experimental-features 'nix-command flakes' -Lv build ".#nixosConfigurations.$hostname.config.system.build.toplevel"
  store_path=$(readlink -f result)
  echo "$hostname> nix copy"
  nix --experimental-features 'nix-command flakes' copy --to ssh://root@"$host" -v "$store_path"

  echo "$hostname> switch-to-configuration switch"
  ssh root@"$host" "$store_path/bin/switch-to-configuration" switch
else
  rsync -az "$(dirname "$0")" root@"$host":nix-config

  echo "> nixos-rebuild switch"
  exec ssh root@"$host" 'nixos-rebuild --flake git+file://`pwd`/nix-config#`hostname` --override-input secrets git+file://`pwd`/nix-config/secrets switch'
fi
