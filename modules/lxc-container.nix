{ hostRegistry, config, pkgs, lib, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/profiles/docker-container.nix")
  ];

  boot = {
    isContainer = true;
    loader = {
      grub.enable = false;
      # /sbin/init
      initScript.enable = true;
    };
  };

  environment.etc."resolv.conf".text = lib.concatMapStrings (ns: ''
    nameserver ${ns}
  '') config.networking.nameservers;

  fileSystems."/" = {
    fsType = "rootfs";
    device = "rootfs";
  };

  nix = {
    useSandbox = false;
    maxJobs = lib.mkDefault 1;
    buildCores = lib.mkDefault 4;
  };

  networking = {
    interfaces.eth0 = {
      useDHCP = false;
      tempAddress = "disabled";
    };
    nameservers = with hostRegistry.hosts.dnscache; [
      ip4
      ip6
      "9.9.9.9"
    ];
    networkmanager.dns = "unbound";
    useDHCP = false;
    useHostResolvConf = false;
    useNetworkd = true;
  };

  services = {
    # Required for remote deployment
    openssh.enable = true;
    resolved.enable = false;
  };

  # Create a few files early before packing tarball for Proxmox architecture/OS detection.
  system.extraSystemBuilderCmds = ''
    mkdir -m 0755 -p $out/bin
    ln -s ${pkgs.bash}/bin/bash $out/bin/sh
    mkdir -m 0755 -p $out/sbin
    ln -s ../init $out/sbin/init
  '';

  systemd.network.networks."40-eth0".networkConfig = {
    IPv6AcceptRA = true;
    LinkLocalAddressing = "ipv6";
  };
}
