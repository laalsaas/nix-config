{ hostRegistry, config, pkgs, lib, ... }:

let

  nginxGlobalLogging = ''
    log_format graylog2_json escape=json '{ "timestamp": "$time_iso8601", '
                     '"remote_addr": "$remote_addr", '
                     '"body_bytes_sent": $body_bytes_sent, '
                     '"request_time": $request_time, '
                     '"response_status": $status, '
                     '"request": "$request", '
                     '"request_method": "$request_method", '
                     '"host": "$host",'
                     '"upstream_cache_status": "$upstream_cache_status",'
                     '"upstream_addr": "$upstream_addr",'
                     '"http_x_forwarded_for": "$http_x_forwarded_for",'
                     '"http_referrer": "$http_referer", '
                     '"http_user_agent": "$http_user_agent" }';

    # replace the hostnames with the IP or hostname of your Graylog2 server
    access_log syslog:server=graylog.server.org:12301 graylog2_json;
    error_log syslog:server=graylog.server.org:12302;
  '';

in {
  # add central logging
  services.journalbeat = {
    enable = false;
    extraConfig = ''
      journalbeat.inputs:
        # Paths that should be crawled and fetched. Possible values files and directories.
        # When setting a directory, all journals under it are merged.
        # When empty starts to read from local journal.
      - paths: []

      journalbeat:
        seek_position: cursor
        cursor_seek_fallback: tail
        write_cursor_state: true
        cursor_flush_period: 5s
        clean_field_names: true
        convert_to_numbers: false
        move_metadata_to_field: journal
        default_type: journal
        kernel: true
      output.logstash:
        # Boolean flag to enable or disable the output module.
        enabled: true
        hosts: ["${config.c3d2.hosts.logging.ip4}:5044"]
    '';
  };

  services.prometheus.exporters.node = lib.mkIf (pkgs.system != "riscv64-linux") {
    enable = true;
    openFirewall = true;
  };
}
