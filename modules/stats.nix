{ lib, config, ... }:

let
  cfg = config.c3d2.hq.statistics;

  isMetal =
    !config.boot.isContainer &&
    !(config ? microvm);

in
{
  options.c3d2.hq.statistics = {
    enable = lib.mkEnableOption "statistics collection";
  };

  config = {
    services.collectd = lib.mkIf cfg.enable {
      enable = true;
      extraConfig = ''
        FQDNLookup false
        Interval 10
      '';
      buildMinimalPackage = true;
      plugins = {
        logfile = ''
          LogLevel info
          File STDOUT
        '';
        network = ''
          Server "grafana.serv.zentralwerk.org" "25826"
        '';
        memory = "";
        processes = "";
        disk = "";
        df = "";
        cpu = "";
        entropy = "";
        load = "";
        swap = "";
        cgroups = "";
        vmem = "";
        interface = "";
      } // lib.optionalAttrs isMetal {
        sensors = "";
        cpufreq = "";
        irq = "";
        ipmi = "";
        thermal = "";
      } // lib.optionalAttrs config.services.nginx.enable {
        nginx = ''
          URL "http://localhost/nginx_status"
        '';
      };
    };
    # Workaround for nixpkgs/master:
    users.users.collectd = {
      isSystemUser = true;
      group = "collectd";
    };
    users.groups.collectd = {};

    services.nginx = lib.mkIf config.services.nginx.enable {
      virtualHosts.localhost.locations."/nginx_status".extraConfig = ''
        stub_status;

        access_log off;
        allow 127.0.0.1;
        allow ::1;
        deny all;
      '';
    };
  };
}
