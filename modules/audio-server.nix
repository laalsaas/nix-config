{ config, lib, pkgs, ... }:

{
  options.c3d2.audioServer = {
    enable = lib.mkEnableOption "Enable PulseAudio and Bluetooth sinks";
  };

  config = lib.mkIf config.c3d2.audioServer.enable {
    sound.enable = true;
    hardware.bluetooth = {
      enable = lib.mkDefault true;
      settings = {
        Policy.AutoEnable = true;
        General.DiscoverableTimeout = 0;
      };
    };

    hardware.pulseaudio = {
      enable = !config.services.pipewire.pulse.enable;
      systemWide = true;
      tcp = {
        enable = true;
        anonymousClients.allowedIpRanges = [
          "127.0.0.0/8"
          "::1/128"
          "fd23:42:c3d2:500::/56"
          "172.22.99.0/24"
          "172.20.72.0/21"
          "2a00:8180:2c00:200::/56"
          "2a0f:5382:acab:1400::/56"
        ];
      };
      zeroconf.publish.enable = true;
      package = (pkgs.pulseaudio.override {
        bluetoothSupport = true;
        advancedBluetoothCodecs = true;
        zeroconfSupport = true;
      }).overrideAttrs (oldAttrs: {
        # one test times out
        doCheck = false;
      });
    };

    services.pipewire = {
      enable = true;
      config.pipewire-pulse =
        let
          default-pipewire-pulse = lib.importJSON (pkgs.path + "/nixos/modules/services/desktops/pipewire/daemon/pipewire-pulse.conf.json");
        in
        default-pipewire-pulse // {
          "context.exec" = default-pipewire-pulse."context.exec" ++ [
            {
              "path" = "pactl";
              "args" = "load-module module-zeroconf-publish";
            }
          ];
          "pulse.properties" = {
            "auth-ip-acl" = [
              "127.0.0.0/8"
              "::1/128"
              "fd23:42:c3d2:500::/56"
              "172.22.99.0/24"
              "172.20.72.0/21"
              "2a00:8180:2c00:200::/56"
              "2a0f:5382:acab:1400::/56"
            ];
            "server.address" = [
              "unix:native"
              "tcp:4713"
            ];
          };
        };
      pulse.enable = true;
    };

    security.rtkit.enable = true;

    # tell Avahi to publish services like Pipewire/PulseAudio
    services.avahi = {
      enable = true;
      publish = {
        enable = true;
        addresses = true;
        userServices = true;
      };
    };

    system.activationScripts.enableLingering = lib.optionalString config.services.pipewire.pulse.enable (''
      rm -r /var/lib/systemd/linger
      mkdir /var/lib/systemd/linger
    '' + lib.optionalString config.c3d2.k-ot.enable ''
      touch /var/lib/systemd/linger/k-ot
    '');

    systemd.services.bluetooth-agent = lib.mkIf config.hardware.bluetooth.enable {
      description = "Allow anyone to pair via Bluetooth";
      wantedBy = [ "multi-user.target" ];
      requires = [ "bluetooth.target" ];
      after = [ "bluetooth.service" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.bluez-tools}/bin/bt-agent -c NoInputNoOutput";
        Restart = "on-failure";
        RestartSec = 60;
      };
    };

    users = {
      groups.pulse-access = { };
      users.k-ot.extraGroups = [
        "pipewire"
        "pulse-access" # required for system wide pulseaudio
      ];
    };
  };
}
