{ tftproots, hostRegistry, lib, pkgs, ... }:
let
  netbootxyzVersion = "2.0.60";
  netbootxyz_efi = pkgs.fetchurl {
    url = "https://github.com/netbootxyz/netboot.xyz/releases/download/${netbootxyzVersion}/netboot.xyz.efi";
    sha256 = "1k9i81iw6lhs1h8qy8yapasqcl31yxl2jxn52ls5anvm477650qk";
  };
  netbootxyz_kpxe = pkgs.fetchurl {
    url = "https://github.com/netbootxyz/netboot.xyz/releases/download/${netbootxyzVersion}/netboot.xyz.kpxe";
    sha256 = "1jr8qwkkj3ccvhdw98fakj07md0nkswy2mlg1rdhcnqzhas7qbj3";
  };
  tftpRoot = pkgs.runCommand "tftproot" {} ''
    mkdir $out

    # PXE for PC
    ln -s ${netbootxyz_efi} $out/netboot.xyz.efi
    ln -s ${netbootxyz_kpxe} $out/netboot.xyz.kpxe

    # generic boot files for pis
    cp -sr ${tftproots.rpi-netboot-tftproot}/* $out/

    # dacbert
    ln -s /var/lib/nfsroot/dacbert/boot $out/${hostRegistry.hosts.dacbert.serial}

    # boot files for specific pis by serial number subdirectories
    ${lib.concatMapStrings (host: ''
      if ! [ -L $out/${hostRegistry.hosts.${host}.serial} ]; then
        ln -s ${tftproots."${host}-tftproot"} $out/${hostRegistry.hosts.${host}.serial}
      fi
    '') (
      builtins.attrNames (
        lib.filterAttrs (_: { serial ? null, ... }: serial != null)
          hostRegistry.hosts
      )
    )}
  '';
in
{
  networking.firewall.enable = false;

  # raspberrypi boot
  services.atftpd = {
    enable = true;
    root = tftpRoot;
  };
}
