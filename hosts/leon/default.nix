#                                               .--.
#                                               `.  \
#                                                 \  \
#                                                  .  \
#                                                  :   .
#                                                  |    .
#                                                  |    :
#                                                  |    |
#  ..._  ___                                       |    |
# `."".`''''""--..___                              |    |
# ,-\  \             ""-...__         _____________/    |
# / ` " '                    `""""""""                  .
# \                                                      L
# (>                                                      \
#/                                                         \
#\_    ___..---.                 MEOW OS                    \
#  `--'         '.                                           \
#                 .                                           \_
#                _/`.                                           `.._
#             .'     -.                                             `.
#            /     __.-Y     /''''''-...___,...--------.._            |
#           /   _."    |    /                ' .      \   '---..._    |
#          /   /      /    /                _,. '    ,/           |   |
#          \_,'     _.'   /              /''     _,-'            _|   |
#                  '     /               `-----''               /     |
#                  `...-'                                       `...-'
#--------------------------------------------------------------------------------


{ config, lib, pkgs, ... }:

{
  microvm = {
    mem = 2048;
    writableStoreOverlay = "/nix/.rw-store";
    volumes = [ {
      image = "nix-store-overlay.img";
      mountPoint = config.microvm.writableStoreOverlay;
      size = 3048;
    } ];
  };
  nix.settings.auto-optimise-store = lib.mkForce false;

  c3d2.deployment = {
    server = "server10";
    mounts = [ "etc" "home" "var"];
    autoNetSetup = true;
  };

  networking = {
    hostName = "leon";
    firewall.enable = true;
  };
  # enable IP routing
  boot.kernel.sysctl."net.ipv4.conf.all.forwarding" = 1;
  boot.kernel.sysctl."net.ipv4.conf.default.forwarding" = 1;

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  c3d2.hq.statistics.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     wget vim python3Full   python310Packages.pip nmap htop wireguard-tools docker-compose
   ];

  virtualisation.docker.enable = true;


  users.users.leon = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" ];
    createHome = true;
    openssh.authorizedKeys.keys = [
	    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIANupx+diz5N8sGZOc7ZXopyPh9HaML8M7Qh70aVVIaJ leon@leons-Air"
	  ];
  };

	networking.firewall = {
    allowedTCPPorts = [ 5000 22 53 80 8080 ];
    allowedUDPPorts = [ 5000 22 53 80 8080 18900 19900 ];  
  };

#_______________________________Begin-VPN1-Server____________________________________
 
 networking.wireguard.interfaces = {
    #Interface. Trusted VPN
	vpn1 = {
      #IP address && Subnet.
      ips = [ "10.10.11.1/24" ];
      #VPN Port.
      listenPort = 18900;
      privateKeyFile = "/etc/wireguard/privatekey";
      #----------------------Start-Routing----------------------------
	  postSetup = ''
        ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s 10.10.11.1/24 -o flpk -j MASQUERADE
      '';

      # This undoes the above command
      postShutdown = ''
        ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s 10.10.11.1/24 -o flpk -j MASQUERADE
      '';
      #----------------------End-Routing----------------------------
     
	 peers = [
           # ------------------leon-------------------------.
        {
		  #leoncloud-vm
		  publicKey = "YhcC/xMYxh7GIyndbgBZ05oE3aTJXK4T7JgZwUDyd08=";
          allowedIPs = [ "10.10.11.4" ];
		}
		{ 
          #My_laptop
		  publicKey = "okZuIQ90J49IJ3s+/dZyLthvo4rR2kclmTL54Ykglio=";
          allowedIPs = [ "10.10.11.2" ];
		}
		{
		  #Myphone
		  publicKey= "OKv8k1ctlcYN9du/lWA65E0+nMpYOhUR8jf+rfQS+BY=";
          allowedIPs = [ "10.10.11.3" ];
        }
		  #-------------------<<B>>--------------------------.
		{
		  #B-Mobile
		  publicKey= "mYnyJZkJ66vqJ5IyN3jmdhoCjuXIfb21c4exN77dVg0=";
          allowedIPs = [ "10.10.11.50" ];
	    }
	    {
		  #B-laptop
		  publicKey= "L2cXR9jXJZIqjwffqm11j3qpm5a+wz3+wlDkkzNNs08=";
          allowedIPs = [ "10.10.11.51" ];
		}
		  #-------------------Killow---------------------------.
		{ 
		  #Killow-Server1
	 	  publicKey= "sg2U9isTU7fFj9dv8lFepbg1EE4s7RissF6ZCZf2cGs=";
          allowedIPs = [ "10.10.11.60" ];
		}
	    {
		  #Killow-Server2
		  publicKey= "tNWJ8FFydI08Ls/faHU1mp0Uf1ZLzZke66DpL6ffoHY=";
          allowedIPs = [ "10.10.11.61" ];
	    }
	    {
		  #Gecol-docker
		  publicKey= "DiA9FKxMDziCt4sEsINIdc8RlM1hq9fA4LVwf062mAE=";
          allowedIPs = [ "10.10.11.62" ];
	    }
		#---------------------joker----------------------------.
	    {
		  #joker-phone
		  publicKey= "FRuPoJ1XVGWTeGO2vZPd4MC0hxjRailAzhMHg3ZegV0=";
          allowedIPs = [ "10.10.11.30" ];
	    }
	   #---------------------black----------------------------.
	    {
		  #black-phone
		  publicKey= "3GEocBTOztOKWtWQFOmI5hynPiPcMMe54kVaHECAG1A=";
          allowedIPs = [ "10.10.11.90" ];
	    }
	    {
		  #black-desktop
		  publicKey= "24TAa1HMXTPaFNfbXLkuxVy305NjSAegMSYjKLH5Kjc=";
          allowedIPs = [ "10.10.11.91" ];
	    }

	  ];
  };
 };
 #-----------------------------END-VPN---------------------------------


#__________________________Begin-VPN2-Server_____________________
 
networking.wireguard.interfaces = {
    #Interface. Untrusted VPN
	vpn2 = {
      #IP address && Subnet.
      ips = [ "10.10.100.1/24" ];

      #VPN Port.
      listenPort = 19900;

	  # Path to the private key file.
      #
      privateKeyFile = "/etc/wireguard/privatekey";

      peers = [
        # -----------------leon-Mac-------------------------.
        { 
          publicKey = "6GRIp7SjHyu5sgqudtgZdN9CKbV3GYtMnwgo06F4ylo=";
          allowedIPs = [ "10.10.100.0/24" ];
        }
	  ];
    };
  };
  
#-----------------------------END-VPN---------------------------------

system.stateVersion = "22.05";
}
