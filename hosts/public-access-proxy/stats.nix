{ pkgs, lib, ... }:
{
  c3d2.hq.statistics.enable = true;

  services.collectd.plugins.exec = ''
    Exec "collectd" "${pkgs.ruby}/bin/ruby" "${./haproxy-stats.rb}"
  '';

  # add a socket that is world-accessible
  services.haproxy.config = ''
    global
      stats socket /run/haproxy/haproxy-stats.sock mode 666
  '';
}
