{ zentralwerk, config, pkgs, lib, ... }:

{
  imports = [
    ./proxy.nix
    ./stats.nix
  ];

  c3d2.deployment = {
    server = "server10";
    mounts = [ "etc" "var"];
  };

  networking.hostName = "public-access-proxy";

  services.proxy = {
    enable = true;
    proxyHosts = [ {
      hostNames = [ "vps1.nixvita.de" "vps1.codetu.be" "nixvita.de" ];
      proxyTo.host = "172.20.73.51";
      matchArg = "-m end";
    } {
      hostNames = [ "auth.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.auth.ip4;
    } {
      hostNames = [ "jabber.c3d2.de" ];
      proxyTo = {
        host = config.c3d2.hosts.jabber.ip4;
      };
      matchArg = "-m end";
    } {
      hostNames = [ "zw.poelzi.org" ];
      proxyTo.host = "172.20.73.162";
      matchArg = "-m end";
    } {
      hostNames = [ "direkthilfe.c3d2.de" ];
      proxyTo = {
        host = config.c3d2.hosts.direkthilfe.ip4;
      };
      matchArg = "-m end";
    } {
      hostNames = [ "staging.dvb.solutions" ];
      proxyTo = {
        host = config.c3d2.hosts.staging-data-hoarder.ip4;
      };
      matchArg = "-m end";
    } {
      hostNames = [ "dvb.solutions" ];
      proxyTo = {
        host = "172.20.73.69";
      };
      matchArg = "-m end";
    } {
      hostNames = [ "bind.serv.zentralwerk.org" ];
      proxyTo.host = config.c3d2.hosts.bind.ip4;
    } {
      hostNames = [ "blogs.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.blogs.ip4;
    } {
      hostNames = [
        "datenspuren.de" "www.datenspuren.de" "ds.c3d2.de" "datenspuren.c3d2.de"
        "c3d2.de" "www.c3d2.de" "c3dd.de" "www.c3dd.de" "cccdd.de" "www.cccdd.de" "dresden.ccc.de" "www.dresden.ccc.de"
        "openpgpkey.c3d2.de"
        "netzbiotop.org" "www.netzbiotop.org"
        "autotopia.c3d2.de"
        "c3d2-web.serv.zentralwerk.org"
      ];
      proxyTo.host = config.c3d2.hosts.c3d2-web.ip4;
    } {
      hostNames = [
        "codimd.c3d2.de"
        "hackmd.c3d2.de"
        "hedgedoc.c3d2.de"
      ];
      proxyTo.host = config.c3d2.hosts.hedgedoc.ip4;
    } {
      hostNames = [ "ftp.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.ftp.ip4;
    } {
      hostNames = [ "gitea.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.gitea.ip4;
    } {
      hostNames = [ "grafana.hq.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.grafana.ip4;
    } {
      hostNames = [
        "hydra.hq.c3d2.de"
        "hydra-ca.hq.c3d2.de"
        "nix-serve.hq.c3d2.de"
      ];
      proxyTo.host = config.c3d2.hosts.hydra.ip4;
    } {
      hostNames = [
        "zentralwerk.org"
        "www.zentralwerk.org"
      ];
      proxyTo.host = config.c3d2.hosts.network-homepage.ip4;
    } {
      hostNames = [
        "kibana.hq.c3d2.de"
        "kibana-es.hq.c3d2.de"
      ];
      proxyTo.host = config.c3d2.hosts.kibana.ip4;
    } {
      hostNames = [ "matemat.hq.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.matemat.ip4;
    } {
      hostNames = [ "mobilizon.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.mobilizon.ip4;
    } {
      hostNames = [ "drkkr.hq.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.pulsebert.ip4;
    } {
      hostNames = [ "scrape.hq.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.scrape.ip4;
    } {
      hostNames = [
        "adsb.hq.c3d2.de"
        "sdr.hq.c3d2.de"
      ];
      proxyTo.host = config.c3d2.hosts.sdrweb.ip4;
    } {
      hostNames = [
        "stream.hq.c3d2.de" "torrents.hq.c3d2.de"
      ];
      proxyTo.host = config.c3d2.hosts.stream.ip4;
    } {
      hostNames = [ "ticker.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.ticker.ip4;
    } {
      hostNames = [ "wiki.c3d2.de" ];
      proxyTo.host = config.c3d2.hosts.mediawiki.ip4;
    } {
      hostNames = [ "zengel.datenspuren.de" ];
      proxyTo.host = config.c3d2.hosts.zengel.ip4;
    } ];
  };

  networking.firewall.allowedTCPPorts = [
    # haproxy
    80 443
    # gemini
    1965
  ];

  # DNS records IN AAAA {www.,}c3d2.de point to this host but
  # gemini:// is served on c3d2-web only
  systemd.services.gemini-forward = {
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [ socat ];
    script = ''
      socat tcp6-listen:1965,fork "tcp6:[${zentralwerk.lib.config.site.net.serv.hosts6.dn42.c3d2-web}]:1965"
    '';
    serviceConfig = {
      ProtectSystem = "strict";
      DynamicUser = true;
    };
  };

  system.stateVersion = "18.09";
}
