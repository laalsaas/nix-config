{ zentralwerk, config, lib, pkgs, ... }:

{
  microvm = {
    mem = 4024;
    writableStoreOverlay = "/nix/.rw-store";
    volumes = [ {
      image = "nix-store-overlay.img";
      mountPoint = config.microvm.writableStoreOverlay;
      size = 32 * 1024;
    } ];
  };

  nix.settings.auto-optimise-store = lib.mkForce false;

  c3d2.deployment = {
    server = "server9";
    mounts = [ "etc" "home" "var" ];
    autoNetSetup = true;
  };

  networking = {
    hostName = "leoncloud";
    firewall.enable = true;
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };
  
  c3d2.hq.statistics.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget
    vim
    python3Full
    python310Packages.pip
    nmap
    htop
    wireguard-tools
    docker-compose 
  ];

  users.users.leon-docker = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" ];
    createHome = true;
    openssh.authorizedKeys.keys = [
	    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJPZoT83l0ogbJpviBs4VmO+NdF4NPtYAnyf8RRSoXsv leon@leon"
		"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIANupx+diz5N8sGZOc7ZXopyPh9HaML8M7Qh70aVVIaJ leon@leons-Air"
	  ];
  };

  networking.firewall = {
    allowedTCPPorts = [ 80 443 22 53 14000 14500 15000  ];
    allowedUDPPorts = [ 18900 53 ];  
  };

#>-----------------docker-------------------------

  virtualisation.docker.enable = true;

#<-----------------docker-------------------------

#>-----------------wireguard client---------------

# Enable WireGuard
  networking.wireguard.interfaces = {
    vpn = {
      ips = [ "10.10.11.4/24" ];
      privateKeyFile = "/etc/nixos/wireguard-keys/private-key";
      peers = [
        {
          publicKey = "w3qegSSuqFTFrGk4XJaWBSwGDOiqbnnAIR9MzwjYVA8=";
          allowedIPs = [ "10.10.11.0/24" ];
          endpoint = "45.158.40.162:18900";
          persistentKeepalive = 25;
        }
      ];
    };
  };

#<-----------------wireguard client---------------

#>-----------------nextcloud----------------------

services.nextcloud = {
    enable = true;
    hostName = "cloud";
    config = {
      dbtype = "pgsql";
      dbuser = "nextcloud";
      dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
      dbname = "nextcloud";
      adminpassFile = "/etc/nixos/next-cloud/pass";
      adminuser = "root";
      extraTrustedDomains = [ "172.20.79.254" "10.10.11.4" ];
    };
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [
      {
        name = "nextcloud";
        ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
      }
    ];
  };

  # ensure that postgres is running *before* running the setup
  systemd.services."nextcloud-setup" = {
    requires = ["postgresql.service"];
    after = ["postgresql.service"];
  };

#<-----------------nextcloud----------------------

  system.stateVersion = "22.05";

}
