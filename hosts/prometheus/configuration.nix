{ config, pkgs, lib, ... }:

{
  imports = [
    ../../../modules/lxc-container.nix
  ];

  networking = {
    hostName = "prometheus";
    firewall = {
      allowedTCPPorts = [ 22 80 443 9090 9091 9093 9094 ];
      enable = true;
    };
  };

  services.prometheus = {
    enable = true;

    alertmanager = {
      enable = true;
      openFirewall = true;
      webExternalUrl = "http://prometheus.serv.zentralwerk.org/alertmanager/";
      listenAddress = "0.0.0.0";
      configuration = {
        "global" = {
          "smtp_smarthost" = "mail.serv.zentralwerk.org:587";
          "smtp_from" = "alertmanager@prometheus.serv.zentralwerk.org";
        };
        "route" = {
          "group_by" = [ "alertname" "alias" ];
          "group_wait" = "30s";
          "group_interval" = "2m";
          "repeat_interval" = "4h";
          "receiver" = "team-admins";
        };
        "receivers" = [{
          "name" = "team-admins";
          # "email_configs" = [
          #   {
          #     "to" = "devnull@example.com";
          #     "send_resolved" = true;
          #   }
          # ];
          # "webhook_configs" = [
          #   {
          #     "url" = "https://example.com/prometheus-alerts";
          #     "send_resolved" = true;
          #   }
          # ];
        }];
      };
    };

    # alertmanagerURL = [ "http://prometheus.serv.zentralwerk.org/alertmanager/" ];

    pushgateway = {
      enable = true;
      web.external-url = "http://prometheus.serv.zentralwerk.org/push/";
    };

    exporters.collectd.enable = true;
    exporters.collectd.openFirewall = true;

    exporters.nginx.enable = true;
    exporters.nginx.openFirewall = true;
  };

  services.nginx = {
    enable = true;

    virtualHosts."prometheus.serv.zentralwerk.org" = {
      # serverAliases = [ "registry.serv.zentralwerk.org" ];
      enableACME = true;
      onlySSL = true;
      locations.".well-known/acme-challenge/" = {
        root = "/var/lib/acme/acme-challenge/.well-known/acme-challenge/";
      };
      locations."/" = { proxyPass = "http://localhost:9090"; };
    };
  };

  system.stateVersion = "20.09"; # Did you read the comment?
}
