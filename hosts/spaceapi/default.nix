{ zentralwerk, ... }:
{
  c3d2.deployment = {
    server = "server10";
    mounts = [ "etc" "var"];
  };

  networking.hostName = "spaceapi";
  networking.firewall.enable = false;

  services.spaceapi = { enable = true; };

  # HACK for ‘ekg-json-0.1.0.6’ nixos-22.05
  nixpkgs.config.allowBroken = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.03"; # Did you read the comment?
}
