{
  system.stateVersion = "22.05";

  c3d2.hq.statistics.enable = true;
  c3d2.deployment = {
    server = "server10";
    mounts = [ "etc" "var" ];
  };
  microvm.mem = 1024;

  networking = {
    hostName = "gnunet";
    firewall.enable = false;
  };

  services.gnunet = {
    enable = true;
    load = {
      # bits/s
      maxNetDownBandwidth = 1000 * 1000 * 1000;
      maxNetUpBandwidth = 1 * 1000 * 1000;
    };
  };
}
