{ zentralwerk, config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  c3d2 = {
    isInHq = true;
    hq.interface = "eno1";
    hq.enableBinaryCache = false;
    k-ot.enable = true;
    autoUpdate = true;
  };

  nixpkgs.config.allowUnfree = true;

  nix = {
    buildCores = 4;
    maxJobs = 4;
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
  };

  boot = {
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
    };
    kernelPackages = pkgs.linuxPackages_latest;
  };

  networking = {
    hostName = "glotzbert";
    interfaces.eno1.useDHCP = true;
  };

  console = {
    font = "${pkgs.terminus_font}/share/consolefonts/ter-u28n.psf.gz";
    keyMap = "de";
  };

  environment.systemPackages = with pkgs; [
    screen
    chromium
    firefox
    mpv
    kodi
    # tracer-game
    bevy_julia
    bevy_mandelbrot
    allcolors
  ];

  systemd.user.services.x11vnc = {
    description = "X11 VNC server";
    wantedBy = [ "graphical-session.target" ];
    partOf = [ "graphical-session.target" ];
    serviceConfig = {
      ExecStart = ''
        ${pkgs.x11vnc}/bin/x11vnc -shared -forever -passwd k-ot
      '';
      RestartSec = 3;
      Restart = "always";
    };
  };

  # TODO: enable
  networking.firewall.enable = false;

  sound.enable = true;

  hardware = {
    opengl = {
      extraPackages = with pkgs; [
        vaapiIntel
        libvdpau-va-gl
        intel-media-driver
      ];
      extraPackages32 = with pkgs.pkgsi686Linux; [
        vaapiIntel
        libvdpau-va-gl
        intel-media-driver
      ];
    };
    pulseaudio = {
      enable = true;
      # Users must be in "audio" group
      systemWide = true;
      support32Bit = true;
      zeroconf = {
        discovery.enable = true;
        publish.enable = true;
      };
      tcp = {
        enable = true;
        anonymousClients.allowAll = true;
      };
      extraConfig = ''
        load-module module-tunnel-sink server=pulsebert.hq.c3d2.de
      '';
      extraClientConf = ''
        default-server = pulsebert.hq.c3d2.de
      '';
    };
  };

  services = {
    xserver = {
      enable = true;
      layout = "de";
      xkbOptions = "eurosign:e";

      displayManager = {
        lightdm = { enable = true; };
        autoLogin = {
          enable = true;
          user = "k-ot";
        };
        defaultSession = "gnome-xorg";
      };
      desktopManager = {
        gnome.enable = true;
        kodi.enable = true;
      };
    };
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  users.groups."k-ot" = { gid = 1000; };
  users.users."k-ot" = {
    group = "k-ot";
    extraGroups = [ "networkmanager" ];
  };

  system.stateVersion = "18.09"; # Did you read the comment?
}
