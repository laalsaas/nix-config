{ zentralwerk, config, lib, pkgs, ... }:

{
  c3d2 = {
    deployment = {
      server = "server10";
      mounts = [ "etc" "home" "var"];
    };
  };

  microvm.mem = 1024;

  boot.kernel.sysctl = {
    # table overflow causing packets from nginx to hedgedoc to drop
    # nf_conntrack: nf_conntrack: table full, dropping packet
    "net.netfilter.nf_conntrack_max" = "32768";
  };

  networking = {
    hostName = "hedgedoc";
    hosts = {
      "2a00:8180:2c00:282::48" = [ "auth.c3d2.de" ];
      "172.20.73.72" = [ "auth.c3d2.de" ];
    };
    firewall.allowedTCPPorts = [ 80 443 ];
  };

  services = {
    hedgedoc = {
      enable = true;
      settings = {
        allowAnonymousEdits = true;
        allowFreeURL = true;
        allowOrigin = [ "hedgedoc.c3d2.de" ];
        csp = {
          enable = true;
          addDefaults = true;
          upgradeInsecureRequest = "auto";
        };
        db = {
          dialect = "postgres";
          host = "/run/postgresql/";
        };
        defaultPermission = "freely";
        domain = "hedgedoc.c3d2.de";
        ldap = {
          url = "ldaps://auth.c3d2.de";
          bindDn = "uid=search,ou=users,dc=c3d2,dc=de";
          bindCredentials = "$bindCredentials";
          searchBase = "ou=users,dc=c3d2,dc=de";
          searchFilter = "(&(objectclass=person)(uid={{username}}))";
          tlsca = "/etc/ssl/certs/ca-certificates.crt";
          useridField = "uid";
        };
        protocolUseSSL = true;
        sessionSecret = "$sessionSecret";
      };
      environmentFile = config.sops.secrets."hedgedoc".path;
    };

    nginx = {
      enable = true;
      enableReload = true;
      virtualHosts = {
        "codimd.c3d2.de" = {
          forceSSL = true;
          enableACME = true;
          locations."/".return = "301 https://hedgedoc.c3d2.de$request_uri";
        };
        "hackmd.c3d2.de" = {
          forceSSL = true;
          enableACME = true;
          locations."/".return = "301 https://hedgedoc.c3d2.de$request_uri";
        };
        "hedgedoc.c3d2.de" = {
          default = true;
          forceSSL = true;
          enableACME = true;
          locations."/".proxyPass = "http://localhost:${toString config.services.hedgedoc.configuration.port}";
        };
      };
    };

    postgresql = {
      enable = true;
      ensureDatabases = [
        "hedgedoc"
      ];
      ensureUsers = [
        {
          name = "hedgedoc";
          ensurePermissions = {
            "DATABASE \"hedgedoc\"" = "ALL PRIVILEGES";
          };
        }
      ];
      package = pkgs.postgresql_14;
    };
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "hedgedoc".owner = config.systemd.services.hedgedoc.serviceConfig.User;
    };
  };
}
