{ hostRegistry, config, lib, pkgs, ... }:
{
  networking.hostName = "riscbert";

  boot = {
    kernelParams = [
      "verbose" "shell_on_fail"
      "elevator=deadline"
    ];
    initrd = {
      network = {
        enable = true;
        flushBeforeStage2 = false;
      };
      supportedFilesystems = lib.mkForce [
        "nfs"
      ];
      # includeDefaultModules = false;
      availableKernelModules = [
        "stmmac-platform"
        "nfs"
        "usbhid"
      ];
    };

    tmpOnTmpfs = true;
  };
  fileSystems."/mnt" = {
    device = "${hostRegistry.hosts.nfsroot.ip4}:/var/lib/nfsroot/riscbert";
    fsType = "nfs";
    options = [ "nfsvers=3" "proto=tcp" "nolock" "hard" "async" "rw" ];
  };

  system.build.bootFiles = pkgs.runCommand "boot-files" {} ''
    mkdir -p $out/boot
    ${config.boot.loader.generic-extlinux-compatible.populateCmd} -c ${config.system.build.toplevel} -d $out/boot
  '';

  services.journald.extraConfig = ''
    Storage=volatile
  '';

  nix = {
    buildCores = 2;
    maxJobs = 1;
    # trustedUsers = [ "client" ];
    extraOptions = ''
      builders-use-substitutes = true
    '';
    daemonCPUSchedPolicy = "idle";
    daemonIOSchedClass = "idle";
  };
  nixpkgs.config.packageOverrides = pkgs: {
    # TODO: https://github.com/NixOS/nixpkgs/pull/189932
    klibc = pkgs.klibc.overrideAttrs (oa: {
      makeFlags = map (s:
        if s == "KLIBCARCH=riscv"
        then "KLIBCARCH=riscv64"
        else s
      ) oa.makeFlags;
    });
  } //
  lib.genAttrs [ "libffi" "libuv" "openssh" "openssl" ] (p: pkgs.${p}.overrideAttrs (oa: { doCheck = false; checkPhase = ""; }));

  system.stateVersion = "22.05";

  fileSystems."/" = {
    device = "/dev/disk/by-label/NIXOS_SD";
    fsType = "ext4";
  };
}
