{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./microvm-staging.nix
  ];

  c3d2 = {
    deployment.microvmBaseZfsDataset = "server10/vm";
    hq.statistics.enable = true;
  };

  boot= {
    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sda";
    };

    kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmpOnTmpfs = true;
    tmpOnTmpfsSize = "80%";
  };

  networking = {
    firewall = {
      enable = true;
      allowedTCPPorts = [ 22 ];
    };
    hostName = "server10";
    # TODO: change that to something more random
    hostId = "10101010";
  };

  services = {
    openssh.enable = true;
    smartd.enable = true;
    zfs.autoScrub.enable = true;
  };

  # static list of microvms from other sources
  microvm.autostart = [
    "data-hoarder"
    "staging-data-hoarder"
  ];

  system.stateVersion = "21.11"; # Did you read the comment?
}
