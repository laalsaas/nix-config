{ config, pkgs, ... }:
let
  microvms = {
    staging-data-hoarder = {
      flakeref = "git+file:///tmp/dvb-nix-config";
    };
  };

  realizeFlake = with pkgs; "${writeScriptBin "realize-flake" ''
    #! ${runtimeShell} -e

    NAME=$1
    if [ $NAME = "staging-data-hoarder" ]; then
      SRC_NIX_CONFIG=https://github.com/dump-dvb/nix-config.git
      SRC_DUMP_DVB=https://github.com/dump-dvb/dump-dvb.nix.git
      BRANCH=master

      DIR_NIX_CONFIG=dvb-nix-config
      DIR_DUMP_DVB=dvb-dump
    else
      echo "Do not know what to do"
      exit 0
    fi

    cd /tmp
    if [ -d $DIR_DUMP_DVB ]; then
      cd $DIR_DUMP_DVB
      git fetch origin
      git reset --hard origin/$BRANCH
    else
      git clone -b $BRANCH --single-branch $SRC_DUMP_DVB $DIR_DUMP_DVB
      cd $DIR_DUMP_DVB
    fi
    git config --global user.email "astro@spaceboyz.net"
    git config --global user.name "Updater"
    nix flake update --commit-lock-file

    cd /tmp
    if [ -d $DIR_NIX_CONFIG ]; then
      cd $DIR_NIX_CONFIG
      git fetch origin
      git reset --hard origin/$BRANCH
    else
      git clone -b $BRANCH --single-branch $SRC_NIX_CONFIG $DIR_NIX_CONFIG
      cd $DIR_NIX_CONFIG
    fi

    nix flake update --commit-lock-file --override-input dump-dvb /tmp/$DIR_DUMP_DVB
  ''}/bin/realize-flake";
in
{
  microvm.autostart = builtins.attrNames microvms;

  systemd.services = {

    "microvm-update@" = {
      description = "Update MicroVMs automatically";
      after = [ "network-online.target" ];
      unitConfig.ConditionPathExists = "/var/lib/microvms/%i";
      serviceConfig = {
        Type = "oneshot";
      };
      path = with pkgs; [ nix git ];
      environment.HOME = config.users.users.root.home;
      scriptArgs = "%i";
      script = ''
        NAME=$1
        ${realizeFlake} $NAME
        /run/current-system/sw/bin/microvm -Ru $NAME
      '';
    };

  } // builtins.foldl' (services: name: services // {
    "microvm-create-${name}" = {
      description = "Create MicroVM ${name} automatically";
      wantedBy = [ "microvms.target" ];
      after = [ "network-online.target" ];
      before = [
        "microvm-tap-interfaces@${name}.service"
        "microvm-virtiofsd@${name}.service"
      ];
      unitConfig.ConditionPathExists = "!/var/lib/microvms/${name}";
      serviceConfig.Type = "oneshot";
      path = [ pkgs.git ];
      environment.HOME = config.users.users.root.home;
      scriptArgs = "${name}";
      script = ''
        ${realizeFlake} ${name}

        /run/current-system/sw/bin/microvm -c ${name} -f "${microvms.${name}.flakeref}"
      '';
    };
  }) {} (builtins.attrNames microvms);

  systemd.timers = builtins.foldl' (timers: name: timers // {
    "microvm-update-${name}" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        Unit = "microvm-update@${name}.service";
        # three times per hour
        OnCalendar = "*:0,20,40:00";
        Persistent = true;
      };
    };
  }) {} (builtins.attrNames microvms);
}
