{ zentralwerk, config, lib, pkgs, ... }:

with lib;

{
  system.stateVersion = "22.05";

  c3d2.hq.statistics.enable = true;
  c3d2.deployment = {
    server = "server10";
    mounts = [ "etc" "var"];
  };

  networking = {
    hostName = "network-homepage";
    firewall.allowedTCPPorts = [ 22 80 443 ];
  };

  services = {
    nginx = rec {
      enable = true;
      virtualHosts."www.zentralwerk.org" = {
        forceSSL = true;
        enableACME = true;
        root = "${zentralwerk.packages.${pkgs.system}.homepage}/share/doc/zentralwerk/www";
      };
      virtualHosts."zentralwerk.org" = virtualHosts."www.zentralwerk.org" // {
        default = true;
      };
    };
  };
}
