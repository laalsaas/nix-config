{ zentralwerk, config, pkgs, lib, tigger, ... }:

{
  c3d2.deployment = {
    # needs to keep just its ssh key for sops-nix
    mounts = [ "etc" "var" ];
  };

  networking.hostName = "mucbot";

  users.users.tigger = {
    createHome = true;
    isNormalUser = true;
    group = "tigger";
  };
  users.groups.tigger = { };
  services.tigger = {
    enable = true;
    user = "tigger";
    group = "tigger";
    inherit (pkgs.mucbot) jid password;
    mucs = [ "c3d2@chat.c3d2.de/Astrobot" "international@chat.c3d2.de/Astrobot" ];
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.09"; # Did you read the comment?
}
