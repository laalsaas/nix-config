{ config, pkgs, lib, zentralwerk, ... }:

{
  c3d2 = {
    deployment = {
      server = "server9";
      mounts = [ "etc" "var"];
    };
  };

  microvm.mem = 1024;
  microvm.shares = [
    {
      tag = "ftp";
      source = "/tank/storage/ftp";
      mountPoint = "/var/www";
      proto = "virtiofs";
      socket = "ftp.socket";
    }
  ];

  networking = {
    hostName = "ftp";
    firewall.allowedTCPPorts = [ 80 443 ];
  };

  users.groups."ftpupload" = { };

  users.users."ftpupload" = {
    home = "/home/ftpupload";
    group = "ftpupload";
    openssh.authorizedKeys.keyFiles = [ ./ftpupload.authorized_keys ];
    isNormalUser = true;
  };

  services.nginx = {
    enable = true;
    recommendedOptimisation = true;
    recommendedTlsSettings = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
    sslDhparam = "${config.security.dhparams.params."nginx".path}";
    defaultListenAddresses = [ "0.0.0.0" "[::]" ];
    additionalModules = [ pkgs.nginxModules.fancyindex ];
  };

  services.nginx.virtualHosts."ftp.c3d2.de" = 
  let
    theme = pkgs.fetchFromGitHub {
      owner = "barrowclift";
      repo = "directory-theme";
      rev = "fca275a3ab1d64e8cdbff7e4d2e1d44eec924e2e";
      sha256 = "sha256-UCQbQ+tyzR/Dpa0t2cogjzBJE+IJ9KAD2dtmq3gbq/U=";
    };
  in
  {
    default = true;
    enableACME = true;
    forceSSL = true;

    locations."/.theme/" = {
      alias = "${theme}/";
    };

    locations."/" = {
      root = "/var/www/ftp.c3d2.de";
      extraConfig = ''
        fancyindex on;
        fancyindex_exact_size off;
        fancyindex_css_href /.theme/style.css;
      '';
    };
  };

  security.dhparams = {
    enable = true;
    stateful = true;
    params."nginx".bits = 4096;
  };

  services.openssh = {
    enable = true;
    extraConfig = ''
      Match User ftpupload
        X11Forwarding no
        ChrootDirectory /var/www/ftp.c3d2.de
        AllowTcpForwarding no
        ForceCommand internal-sftp
    '';
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    secrets = { };
  };

  system.stateVersion = "22.05";
}
