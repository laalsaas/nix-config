{ zentralwerk, config, lib, pkgs, ... }:

{
  c3d2 = {
    deployment = {
      server = "server10";
      mounts = [ "etc" "home" "var"];
    };
  };

  system.stateVersion = "22.05";

  networking = {
    hostName = "auth";
    hosts = {
      # required for ldaps connection over localhost
      "::1" = [ "auth.c3d2.de" ];
      "127.0.0.1" = [ "auth.c3d2.de" ];
    };
    firewall.allowedTCPPorts = [
      80  # http
      443 # https
      636 # ldaps
    ];
  };

  services = {
    nginx = {
      enable = true;
      virtualHosts."auth.c3d2.de" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        locations = {
          "/dex".proxyPass ="http://localhost:${toString config.services.portunus.dex.port}";
          "/" = {
            proxyPass = "http://localhost:${toString config.services.portunus.port}";
          };
        };
      };
    };

    portunus = {
      enable = true;
      dex = {
        # enable = true;
      };
      domain = "auth.c3d2.de";
      ldap = {
        suffix = "dc=c3d2,dc=de";
        tls = true;
      };
      seedPath = config.sops.secrets."portunus/seed".path;
    };
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
    secrets."portunus/seed" = {
      group = config.services.portunus.group;
      owner = config.services.portunus.user;
    };
  };
}
