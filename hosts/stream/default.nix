{ zentralwerk, config, pkgs, ... }:
let
  authFile = pkgs.writeText "htpasswd" "k-ot:sawCOTsl/fIUY";
  mac = {
    pub = "DE:91:C7:51:D1:C5";
    serv = "C6:40:E0:21:9B:A4";
  };
in
{
  networking.hostName = "stream";
  c3d2.hq.statistics.enable = true;
  c3d2.deployment = {
    server = "server9";
    autoNetSetup = false;
    mounts = [];
  };
  microvm.mem = 2048;
  microvm.interfaces = [ {
    type = "tap";
    id = "pub-stream";
    mac = mac.pub;
  } {
    type = "tap";
    id = "serv-stream";
    mac = mac.serv;
  } ];
  microvm.shares = map (name: {
    source = "/tank/storage/stream/${name}";
    mountPoint = "/${name}";
    tag = name;
    proto = "virtiofs";
    socket = "${name}.socket";
  }) [ "etc" "home" "var" ];

  systemd.network = {
    enable = true;

    # On the serv network I have a static IPv4 and only a route to the
    # rest of the network so that I am reachable by
    # public-access-proxy.

    links."00-serv" = {
      matchConfig.MACAddress = mac.serv;
      linkConfig.Name = "serv";
    };

    networks."00-serv" = {
      matchConfig.MACAddress = mac.serv;
      networkConfig.IPv6AcceptRA = false;
      # try harder disabling global ipv6
      networkConfig.LinkLocalAddressing = "no";
      addresses = [ {
        addressConfig.Address = "${config.c3d2.hosts.stream.ip4}/${toString zentralwerk.lib.config.site.net.serv.subnet4Len}";
      } ];
      routes = [ {
        routeConfig = {
          Destination = "172.20.0.0/14";
          Gateway = config.c3d2.hosts.serv-gw.ip4;
        };
      } ];
    };

    # On the pub network I am a normal client.

    links."00-pub" = {
      matchConfig.MACAddress = mac.pub;
      linkConfig.Name = "pub";
    };

    networks."01-pub" = {
      matchConfig.MACAddress = mac.pub;
      networkConfig.DHCP = "ipv4";
      networkConfig.IPv6AcceptRA = true;
    };
  };
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  services.peerflix.enable = true;
  systemd.services.peerflix.serviceConfig = {
    Restart = "always";
    RestartSec = 60;
  };
  services.jackett.enable = true;

  services.nginx = {
    enable = true;
    virtualHosts."stream.hq.c3d2.de" = {
      default = true;
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "http://127.0.0.1:9000";
        extraConfig = ''
          auth_basic "Stream";
          auth_basic_user_file ${authFile};
        '';
      };
    };
    virtualHosts."torrents.hq.c3d2.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "http://127.0.0.1:9117";
        extraConfig = ''
          auth_basic "Torrents";
          auth_basic_user_file ${authFile};
        '';
      };
    };
  };

  system.stateVersion = "22.05";
}
