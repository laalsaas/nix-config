# Our bare-metal hydra is a server in the nomad cluster, it is not a
# client and therefore does not run cluster jobs
{ zentralwerk, config, pkgs, ... }:
let
  inherit (config.networking) hostName;
  ipv4Addr = zentralwerk.lib.config.site.net.serv.hosts4.${hostName};
in
{
  services.nomad = {
    enable = true;
    # nomad<1.3 (default in nixos 22.05) is incompatible with cgroups-v2
    package = pkgs.nomad_1_3;
    enableDocker = false;

    settings = {
      datacenter = "c3d2";
      plugin.raw_exec.config.enabled = true;
      server = {
        enabled = true;
        bootstrap_expect = 3;
        server_join = {
          retry_join = map (server:
            zentralwerk.lib.config.site.net.cluster.hosts4.${server}
          ) [ "server9" "server10" ];
        };
      };
    };
  };
}
