{ zentralwerk, config, pkgs, lib, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./network.nix
    ./hydra.nix
    ./updater.nix
    ./nomad-server.nix
    ../../modules/c3d2.nix
  ];

  nixpkgs.config.allowUnfree = true;

  security.pam.enableSSHAgentAuth = true;

  services.openssh = {
    enable = true;
    passwordAuthentication = false;
  };

  programs.mosh.enable = true;

  nix = {
    sshServe.enable = true;
    trustedUsers = [ "@wheel" ];
    settings = {
      auto-optimise-store = true;
      cores = 20;
      keep-outputs = true;
      max-jobs = 8;
      # restore default overridden by harmonia
      allowed-users = lib.mkForce [ "*" ];
    };
  };

  boot = {
    tmpOnTmpfs = true;
    tmpOnTmpfsSize = "80%";
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = [ "kvm-intel" ];
    kernelParams = [ "mitigations=off" "preempt=none" ];
    # For cross-building
    binfmt.emulatedSystems = [ "armv6l-linux" "armv7l-linux" "aarch64-linux" "riscv32-linux" "riscv64-linux" ];
  };

  networking = {
    hostName = "hydra";
    firewall.enable = false;
    nameservers = [ "172.20.73.8" "9.9.9.9" ];
  };

  c3d2.hq.statistics.enable = true;
  services.smartd.enable = true;

  system.stateVersion = "20.09"; # Did you read the comment?
}
