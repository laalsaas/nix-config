{ self, hostRegistry, hydra-ca, config, lib, pkgs, ... }:

let
  cachePort = 5000;

in
{
  containers = {
    hydra-ca = {
      autoStart = true;
      config = { ... }: {
        imports = [
          hydra-ca.nixosModules.hydra
        ];

        environment.systemPackages = with pkgs; [ git ];

        networking.firewall.allowedTCPPorts = [ 3001 ];

        nix = {
          settings = {
            allowed-uris = "https://gitea.c3d2.de/ https://github.com/ https://gitlab.com/ ssh://gitea@gitea.c3d2.de/";
            builders-use-substitutes = true;
            experimental-features = "ca-derivations nix-command flakes";
            extra-substituters = "https://cache.ngi0.nixos.org/";
            extra-trusted-public-keys = "cache.ngi0.nixos.org-1:KqH5CBLNSyX184S9BKZJo1LxrxJ9ltnY2uAs5c/f1MA=";
            substituters = [
              "https://cache.ngi0.nixos.org/"
            ];
            trusted-public-keys = [
              "cache.ngi0.nixos.org-1:KqH5CBLNSyX184S9BKZJo1LxrxJ9ltnY2uAs5c/f1MA="
            ];
          };
        };

        nixpkgs = {
          # config.contentAddressedByDefault = true;
          overlays = [ self.overlay ];
        };

        services = {
          hydra-dev = lib.recursiveUpdate config.services.hydra-dev {
            hydraURL = "https://hydra-ca.hq.c3d2.de";
            port = 3001;
          };
        };

        system.stateVersion = "22.05"; # Did you read the comment? No.
      };
      hostAddress = "192.168.100.1";
      localAddress = "192.168.100.2";
      privateNetwork = true;
    };
  };

  networking.nat = {
    enable = true;
    externalInterface = "serv";
    internalInterfaces = [ "ve-hydra-ca" ];
  };

  nix = {
    buildMachines = [{
      hostName = "client@dacbert.hq.c3d2.de";
      system = lib.concatStringsSep "," [
        "aarch64-linux" "armv6l-linux" "armv7l-linux"
      ];
      supportedFeatures = [ "kvm" "nixos-test" ];
      maxJobs = 1;
    }];
    daemonCPUSchedPolicy = "idle";
    daemonIOSchedClass = "idle";
    daemonIOSchedPriority = 7;
    settings = {
      allowed-uris = "http:// https:// ssh://";
      builders-use-substitutes = true;
      experimental-features = "ca-derivations nix-command flakes";
    };
    trustedUsers = [ "hydra" "root" ];
  };

  services = {
    hydra-dev = {
      enable = true;
      buildMachinesFiles = [
        "/etc/nix/machines"
        "/var/lib/hydra/machines"
      ];
      hydraURL = "https://hydra.hq.c3d2.de";
      logo = ./c3d2.svg;
      minimumDiskFree = 50;
      minimumDiskFreeEvaluator = 50;
      notificationSender = "hydra@spam.works";
      useSubstitutes = true;
      extraConfig =
        let
          key = config.sops.secrets."nix-serve/secretKey".path;
        in
        ''
          binary_cache_secret_key_file = ${key}
          evaluator_workers = 4
          evaluator_max_memory_size = 2048
          max_output_size = ${toString (5*1024*1024*1024)} # sd card and raw images
          store_uri = auto?secret-key=${key}&write-nar-listing=1&ls-compression=zstd&log-compression=zstd
          upload_logs_to_binary_cache = true
        '';
    };

    # A rust nix binary cache
    harmonia = {
      enable = true;

      settings = {
        bind = "127.0.0.1:${toString cachePort}";
        workers = "20";
        max_connection_rate = 1024;
        priority = 30;
        sign_key_path = config.sops.secrets."nix-serve/secretKey".path;
      };
    };

    nginx =
      let
        hydraVhost = {
          forceSSL = true;
          enableACME = true;
          locations."/".proxyPass = "http://localhost:${toString config.services.hydra.port}";
        };
      in
      {
        enable = true;
        virtualHosts = {
          "hydra.hq.c3d2.de" = hydraVhost // {
            default = true;
          };
          "hydra-ca.hq.c3d2.de" = hydraVhost // {
            locations."/".proxyPass = "http://192.168.100.2:3001";
          };
          "hydra.serv.zentralwerk.org" = hydraVhost;
          "nix-serve.hq.c3d2.de" = {
            forceSSL = true;
            enableACME = true;
            locations."/".proxyPass = "http://localhost:${toString cachePort}";
          };
        };
      };
    resolved.enable = false;
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets."nix-serve/secretKey".mode = "0444";
  };

  systemd.services = {
    hydra-evaluator.serviceConfig = {
      CPUWeight = 2;
      MemoryHigh = "64G";
      MemoryMax = "64G";
      MemorySwapMax = "64G";
    };

    hydra-init.preStart = let
      makesSenseForQemuUser = feature:
        ! (builtins.elem feature [ "kvm" "benchmark" ]);
      # strips features that don't make sense on qemu-user
      extraPlatformSystemFeatures =
        builtins.filter makesSenseForQemuUser config.nix.settings.system-features;
    in ''
      cat << EOF > ~/machines
      localhost x86_64-linux - ${toString config.nix.settings.max-jobs} 10 ${lib.concatStringsSep "," config.nix.settings.system-features} -
      hydra@localhost ${lib.concatStringsSep "," config.nix.settings.extra-platforms} - ${toString config.nix.settings.max-jobs} 10 ${lib.concatStringsSep "," extraPlatformSystemFeatures} -
      EOF
    '';

    nix-daemon.serviceConfig = {
      CPUWeight = 5;
      MemoryHigh = "64G";
      MemoryMax = "64G";
      MemorySwapMax = "64G";
    };
  };
}
