{ zentralwerk, config, lib, pkgs, ... }:

{
  c3d2 = {
    deployment = {
      server = "server10";
      mounts = [ "etc" "home" "var"];
    };
  };

  microvm.mem = 8 * 1024;

  networking = {
    hostName = "factorio";
  };

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "factorio-headless"
  ];

  services = {
    factorio = {
      enable = true;
      admins = [ "SuperSandro2000" ];
      autosave-interval = 10;
      description = "Keep your Kolle Mate ready!";
      game-name = "C3D2 Server";
      game-password = "k-otk-ot";
      lan = true;
      nonBlockingSaving = true;
      openFirewall = true;
    };
  };

  system.stateVersion = "22.05";
}
