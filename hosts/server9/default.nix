{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];
  boot= {
    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sdc";
    };

    # kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmpOnTmpfs = true;
    tmpOnTmpfsSize = "80%";
  };

  networking = {
    hostName = "server9";
    hostId = "09090909";
  };
  system.stateVersion = "21.11";
  services.openssh.enable = true;

  services.zfs.autoScrub.enable = true;
  services.smartd.enable = true;

  c3d2 = {
    deployment.microvmBaseZfsDataset = "tank/storage";
    hq.statistics.enable = true;
  };

  # XXX: enable for zw-ev and poelzi-ha until we find a better solution
  virtualisation.libvirtd = {
    enable = true;
    onShutdown = "shutdown";
  };
}
