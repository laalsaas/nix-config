{ zentralwerk, config, pkgs, lib, modulesPath, ... }:

let
  restartServices = [ "grafana" "influxdb" ];
in {
  microvm.mem = 4096;
  c3d2.deployment = {
    server = "server10";
    mounts = [ "etc" "home" "var"];
  };
  c3d2.isInHq = false;

  services.openssh.enable = true;
  # noXlibs breaks cairo:
  environment.noXlibs = false;

  networking.hostName = "grafana";

  # http https influxdb
  networking.firewall.allowedTCPPorts = [ 80 443 8086 ];
  # collectd
  networking.firewall.allowedUDPPorts = [ 25826 ];

  services.nginx = {
    enable = true;
    virtualHosts = {
      "grafana.hq.c3d2.de" = {
        default = true;
        enableACME = true;
        forceSSL = true;
        locations = { "/".proxyPass = "http://localhost:3000/"; };
      };
    };
  };
  services.grafana = {
    enable = true;
    domain = "grafana.hq.c3d2.de";
    auth.anonymous = {
      enable = true;
      org_name = "Chaos";
    };
    users.allowSignUp = false;

    provision = {
      enable = true;
      # curl https://root:SECRET@grafana.hq.c3d2.de/api/datasources | jq > hosts/grafana/datasources.json
      datasources = map (datasource: {
        inherit (datasource) name type access orgId url password user database isDefault jsonData;
      }) (with builtins; fromJSON (readFile ./datasources.json));
      # for id in `curl https://root:SECRET@grafana.hq.c3d2.de/api/search | jq -j 'map(.uid) | join(" ")'`; do curl https://root:SECRET@grafana.hq.c3d2.de/api/dashboards/uid/$id | jq .dashboard > hosts/grafana/dashboards/$id.json;done
      dashboards = [
        { options.path = ./dashboards; }
      ];
    };
  };
  services.influxdb = let
    collectdTypes = pkgs.runCommand "collectd-types" {} ''
      mkdir -p $out/share/collectd
      cat ${config.services.collectd.package}/share/collectd/types.db >> $out/share/collectd/types.db
      echo "stations  value:GAUGE:0:U" >> $out/share/collectd/types.db
    '';
  in {
    enable = true;
    extraConfig = {
      logging.level = "debug";
      collectd = [{
        enabled = true;
        database = "collectd";
        typesdb = "${collectdTypes}/share/collectd/types.db";
        # create retention policy "30d" on collectd duration 30d replication 1 default
        retention-policy = "30d";
      }];
    };
  };
  systemd.services =
    builtins.foldl' (services: service:
      services // {
        "${service}".serviceConfig = {
          RestartSec = 60;
          Restart = "always";
        };
      }
    ) {} restartServices
    // {
      # work around our slow storage that can't keep up
      influxdb.serviceConfig.LimitNOFILE = "1048576:1048576";
      influxdb.serviceConfig.TimeoutStartSec = "infinity";
    };
  environment.systemPackages = with pkgs; [ influxdb ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.09"; # Did you read the comment?
}
