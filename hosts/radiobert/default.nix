{ zentralwerk, config, lib, pkgs, ... }:

{
  imports = [
    ./soapysdr.nix
    ./readsb.nix
  ];

  c3d2 = {
    isInHq = false;
    hq.statistics.enable = true;
    pi-sensors = if true then [] else [ {
      type = "dht22";
      pin = 17;
      location = "Schrank";
    } {
      type = "dht22";
      pin = 23;
      location = "Aussen";
    } ];

    # unless you automate walking up to the roof, do never enable
    # automatic updates for this machine!
    autoUpdate = lib.mkForce false;
  };

  boot = {
    loader = {
      grub.enable = false;
      raspberryPi = {
        enable = true;
        version = 4;
        # TODO: this machine actually has u-boot setup but that's no
        # longer working with nixos-22.05. DO NOT REBOOT! :-)
        # https://github.com/NixOS/nixpkgs/pull/112677
        # uboot.enable = true;
        firmwareConfig = ''
          gpu_mem=192
          dtparam=audio=on
        '';
      };
    };

    kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = lib.mkForce [
      "console=tty0"
      # allow GPIO access
      "iomem=relaxed" "strict-devmem=0"
    ];
    # No ZFS on latest kernel:
    supportedFilesystems = lib.mkForce [ "vfat" "ext4" ];

    tmpOnTmpfs = true;
    extraModulePackages = [ ];
    initrd = {
      availableKernelModules = [ "usbhid" ];
      kernelModules = [ ];
    };
    kernelModules = [ ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };

    "/boot/firmware" = {
      device = "/dev/disk/by-label/FIRMWARE";
      fsType = "vfat";
    };
  };

  swapDevices = [ ];

  hardware = {
    deviceTree.enable = false;
    enableRedistributableFirmware = true;
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";

  nixpkgs.config.packageOverrides = pkgs: {
    makeModulesClosure = x:
      # prevent kernel install fail due to missing modules
      pkgs.makeModulesClosure (x // { allowMissing = true; });
  };

  nix = {
    buildCores = 4;
    maxJobs = 2;
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
  };

  networking = {
    hostName = "radiobert"; # Define your hostname.
    useDHCP = false;
    interfaces.eth0.ipv4.addresses = [{
      address = config.c3d2.hosts."${config.networking.hostName}".ip4;
      prefixLength = zentralwerk.lib.config.site.net.serv.subnet4Len;
    }];
    defaultGateway = "172.20.73.1";
    firewall.enable = false;
    nameservers = [ "172.20.73.8" "9.9.9.9" ];
  };

  environment.systemPackages = with pkgs; [
    libraspberrypi
    raspberrypi-eeprom
    vim
    wget
  ];

  programs.tmux.enable = true;

  services = {
    # Do not log to flash:
    journald.extraConfig = ''
      Storage=volatile
    '';
    openssh = {
      enable = true;
    };
    # Allow access to USB
    udev.extraRules = ''
      SUBSYSTEM=="usb", MODE:="0666"
    '';
  };

  system.stateVersion = "21.05"; # Did you read the comment?
}
