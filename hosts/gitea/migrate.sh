#/usr/bin/env bash -e

DUMP=gitea-dump-1633035257
USER=gitea
DATABASE=gitea

cd /tmp/
unzip ${DUMP}.zip
unzip gitea-repo.zip

systemctl stop gitea

rm -r /var/lib/gitea/repositories/*
mv gitea-repositories/* /var/lib/gitea/repositories/
chown -R gitea:gitea /var/lib/gitea

sudo -u gitea psql -U $USER -d $DATABASE < gitea-db.sql

systemctl start gitea
