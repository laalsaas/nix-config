## Migration from [imbert to zentralwerk](https://codimd.c3d2.de/inbert-2021)

[based on https://docs.gitea.io/en-us/backup-and-restore/](https://docs.gitea.io/en-us/backup-and-restore/)

### @imbert
```shell
sudo -u git  gitea dump -c /etc/gitea/app.ini
```

### @gitea.hq.c3d2.de (lxc 315 @server6)

- copied `gitea-dump-*.zip` from imbert to `/tmp/`

```shell
/etc/nixos/migrate.sh
```

Check consistency:

```shell
su gitea
cd
export GITEA_WORK_DIR=/var/lib/gitea
/nix/store/*-gitea-1.15.2/bin/gitea doctor --all
```

#### Fix problems caused by database schema changes between Gitea 1.8.3 and 1.15.2

2 Factor Auth didn't work, but was only used by 2 users anyway. We delete the old settings:

```sql
delete from two_factor;
```

There is a new column `repository.owner_name` that needs be set. Otherwise the web frontend displayed links starting with `//`.

Before fixing, we checked the `owner_names` queried by joining via `"user".id = repo.owner_id`:

```sql
select "user".lower_name, repo.owner_name, repo.lower_name from repository as repo inner join "user" on "user".id = repo.owner_id;
```

```sql
UPDATE repository
SET owner_name = map.name
FROM (SELECT "user".lower_name AS name, repository.owner_id AS id
      FROM repository INNER JOIN "user" ON "user".id = repository.owner_id
     ) AS map
WHERE map.id = repository.owner_id;
```

#### Problems with old logins

Till now `PASSWORD_HASH_ALGO` `argon2` was used, but seems not to work in the new version.
Using the password recovery works.
