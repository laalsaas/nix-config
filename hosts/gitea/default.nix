{ config, pkgs, lib, zentralwerk, ... }:

{
  c3d2 = {
    deployment = {
      server = "server10";
      mounts = [ "etc" "home" "var"];
    };
  };

  microvm.mem = 4 * 1024;

  networking = {
    hostName = "gitea";
    hosts = {
      "2a00:8180:2c00:282::48" = [ "auth.c3d2.de" ];
      "172.20.73.72" = [ "auth.c3d2.de" ];
    };
    firewall.allowedTCPPorts = [ 80 443 2222 ];
  };

  services = {
    gitea = rec {
      enable = true;
      appName = "Gitea: with a cup of Kolle Mate";
      domain = "gitea.c3d2.de";
      rootUrl = "https://${domain}/";

      database = {
        type = "postgres";
      };

      repositoryRoot = "/var/lib/gitea/repositories";

      disableRegistration = true;

      lfs.enable = true;

      log.level = "Info";

      dump = {
        ## Is a nice feature once we have a dedicated backup storage.
        ## For now it is disabled, since it delays `nixos-rebuild switch`.
        enable = false;
        backupDir = "/var/lib/gitea/dump";
      };

      settings = {
        cors = {
          ALLOW_DOMAIN = config.services.gitea.domain;
          ENABLED = true;
          SCHEME = "https";
        };
        cron = {
          ENABLED = true;
        };
        "cron.delete_generated_repository_avatars" = {
          ENABLED = true;
        };
        "cron.delete_old_actions" = {
          ENABLED = true;
        };
        "cron.delete_old_system_notices" = {
          ENABLED = true;
        };
        "cron.repo_health_check" = {
          TIMEOUT = "120s";
        };
        "cron.resync_all_sshkeys" = {
          ENABLED = true;
          RUN_AT_START = true;
        };
        database = {
          LOG_SQL = false;
        };
        indexer = {
          REPO_INDEXER_ENABLED = true;
        };
        log = {
          LEVEL = "Info";
          DISABLE_ROUTER_LOG = true;
        };
        mailer = {
          ENABLED = true;
          FROM = "gitea@c3d2.de";
          MAILER_TYPE = "sendmail";
          SENDMAIL_PATH = "/run/wrappers/bin/sendmail";
          SENDMAIL_ARGS = "--";
        };
        other = {
          SHOW_FOOTER_VERSION = false;
        };
        picture = {
          # this also disables libravatar
          DISABLE_GRAVATAR = false;
          ENABLE_FEDERATED_AVATAR = true;
          GRAVATAR_SOURCE = "libravatar";
          REPOSITORY_AVATAR_FALLBACK = "random";
        };
        server = {
          ENABLE_GZIP = true;
          SSH_AUTHORIZED_KEYS_BACKUP = false;
          SSH_DOMAIN = domain;
        };
        service = {
          NO_REPLY_ADDRESS = "no_reply@c3d2.de";
          REGISTER_EMAIL_CONFIRM = true;
          ENABLE_NOTIFY_MAIL = true;
        };
        session = {
          COOKIE_SECURE = lib.mkForce true;
          PROVIDER = "db";
          SAME_SITE = "strict";
        };
        "ssh.minimum_key_sizes" = {
          ECDSA = -1;
          RSA = 4095;
        };
        time = {
          DEFAULT_UI_LOCATION = config.time.timeZone;
        };
        ui = {
          DEFAULT_THEME = "arc-green";
          EXPLORE_PAGING_NUM = 25;
          FEED_PAGING_NUM = 50;
          ISSUE_PAGING_NUM = 25;
        };
      };
    };

    nginx = {
      enable = true;
      virtualHosts."gitea.c3d2.de" = {
        forceSSL = true;
        enableACME = true;
        locations."/".proxyPass = "http://localhost:3000";
      };
    };

    openssh = {
      enable = true;
      extraConfig = ''
        Match User gitea
          AllowAgentForwarding no
          AllowTcpForwarding no
          PermitTTY no
          X11Forwarding no
      '';
    };
  };

  programs.msmtp = {
    enable = true;
    accounts.default = {
      host = "mail.c3d2.de";
      port = 587;
      tls = true;
      tls_starttls = true;
      auth = false;
      domain = "gitea.c3d2.de";
      from = "mail@c3d2.de";
    };
  };

  environment.systemPackages = with pkgs; [ postgresql unzip ]; # used to restore database dumps

  system.stateVersion = "21.11";
}
