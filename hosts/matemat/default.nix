{ zentralwerk, config, lib, pkgs, ... }:

{
  c3d2 = {
    deployment = {
      server = "server10";
      mounts = [ "etc" "home" "var"];
    };
  };

  microvm.mem = 2 * 1024;

  system.stateVersion = "22.05";

  networking = {
    hostName = "matemat";
    firewall.allowedTCPPorts = [ 80 443 ];
  };

  services = {
    nginx = {
      enable = true;
      virtualHosts."matemat.hq.c3d2.de" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://localhost:3000";
          extraConfig = ''
            satisfy any;
            auth_basic secured;
            auth_basic_user_file ${pkgs.matemat-auth};
            allow 2a00:8180:2c00:200::/56;
            allow 2a0f:5382:acab:1400::/56;
            allow fd23:42:c3d2:500::/56;
            allow 30c:c3d2:b946:76d0::/64;
            allow 172.22.99.0/24;
            allow 172.20.72.0/21;
            deny all;
          '';
        };
      };
    };
    yammat.enable = true;
  };

  programs.msmtp = {
    enable = true;
    accounts.default = {
      host = "mail.c3d2.de";
      port = 587;
      tls = true;
      tls_starttls = true;
      auth = false;
      domain = "matemat.hq.c3d2.de";
      from = "nek0@c3d2.de";
    };
  };
}
