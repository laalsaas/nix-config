{ pkgs, ... }:

let home = "/var/spool/eris";
in {
  services.syndicate.eris = {
    enable = true;
    user = "eris";
    config = [
      (pkgs.writeText "eris.pr" ''
        <require-service <daemon eris-resolver>>
        <daemon eris-resolver {
          argv: [ "${pkgs.nimPackages.eris}/bin/eriscmd" "resolver" ]
          protocol: text/syndicate
        }>

        ? <service-object <daemon eris-resolver> ?cap> $cap [
          <coap-server "::" 5683 #{Get Put}>
          <tkrzw "${home}/eris.tkh" #{Get Put} >
        ]
      '')
    ];
  };

  users.groups.eris = { };
  users.users.eris = {
    isSystemUser = true;
    group = "eris";
    inherit home;
  };
}
