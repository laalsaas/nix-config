{ pkgs, ... }:
let
  domain = "zengel.datenspuren.de";
in {
  networking.hostName = "zengel";
  microvm.mem = 1024;
  c3d2.deployment = {
    server = "server10";
    mounts = [ "etc" "home" "var"];
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];

  services.engelsystem = {
    enable = true;
    domain = domain;
    createDatabase = true;
    package = pkgs.engelsystem.override { php = pkgs.php74; };
    config = {
      database = {
        host = "localhost";
        database = "engelsystem";
        username = "engelsystem";
      };
      timezone = "Europe/Berlin";
      signup_requires_arrival = true;
      autoarrive = true;
      signup_advance_hours = 0;
      last_unsubscribe = 24;
      enable_dect = false;
      enable_planned_arrival = false;
      enable_tshirt_size = false;
      enable_goody = false;
      max_freeloadable_shifts = 20;
      night_shifts.enabled = false;
      default_locale = "de_DE";
      footer_items.Contact = "mailto:mail@c3d2.de";
      footer_items.FAQ = "https://www.c3d2.de/kontakt.html";
      email = {
        driver = "smtp";
        from.address = "mail@c3d2.de";
        from.name = "Engelsystem";
        host = "mail.c3d2.de";
        port = 587;
        tls = true;
        #sendmail = "${pkgs.ssmtp}/bin/sendmail -bs";
      };
    };
  };
  services.phpfpm.phpPackage = pkgs.php74;
  services.nginx = {
    enable = true;
    virtualHosts."${domain}" = {
      default = true;
      forceSSL = true;
      enableACME = true;
    };
  };

  #  services.ssmtp = {
  #    enable = true;
  #    root = "mail@c3d2.de";
  #    useTLS = true;
  #    useSTARTTLS = true;
  #    hostName = "mail.c3d2.de:587";
  #    domain = "direkthilfe.c3d2.de";
  #    settings = {
  #      hostname = "direkthilfe.serv.zentralwerk.org";
  #    };
  #  };

  system.stateVersion = "22.05";
}
