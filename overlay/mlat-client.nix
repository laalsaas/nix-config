{ buildPythonApplication, fetchFromGitHub }:

buildPythonApplication rec {
  pname = "mlat-client";
  version = "0.3.9";

  src = fetchFromGitHub {
    owner = "adsbxchange";
    repo = "mlat-client";
    rev = "v${version}";
    sha256 = "0zqm9g6sg3mzq8x809x9kicc9mqpkh1ndb0xfapb3hkz5d5dnm6z";
  };
}
