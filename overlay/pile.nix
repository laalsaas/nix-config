{ fetchgit, rustPlatform }:

let
  pile = fetchgit {
    url = "https://github.com/astro/pile.git";
    sha256 = "sha256-z4xNUGmP35ZBZUpgozQHANZniADfmwEoclnEwNlvAC4=";
  };
in
rustPlatform.buildRustPackage {
  name = "ledball";
  version = "0.0.0";

  src = "${pile}/ledball";

  cargoSha256 = "0zyfbf3gph8gqab07fmm5a7x5slapsqn8ck6isp53fsa7ljnagjy";

  cargoBuildFlags = [ "--examples" ];

  postInstall = ''
    mkdir -p $out/bin

    for f in target/*/release/examples/*; do
      if [ -f $f ] && [ -x $f ]; then
        cp $f $out/bin/
      fi
    done
  '';
}
