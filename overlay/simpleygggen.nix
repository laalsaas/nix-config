{ lib, stdenv, fetchurl, cmake, libsodium, }:

stdenv.mkDerivation rec {
  pname = "SimpleYggGen-CPP";
  version = "5.1";

  src = fetchurl {
    url = "https://notabug.org/acetone/${pname}/archive/${version}.tar.gz";
    hash = "sha256-z5Wtmw2vq3qRjNSOJ0xlxdmB5g2Oa+UQn1Awt7+R/XY=";
  };

  nativeBuildInputs = [ cmake ];

  buildInputs = [ libsodium ];

  installPhase = ''
    install -D src/sygcpp $out/bin/$pname
  '';

  meta.maintainers = [ lib.maintainers.ehmry ];
}
