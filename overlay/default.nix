{ nixos-unstable
, tracer
, bevy-julia
, bevy-mandelbrot
}:

final: prev:

let
  pkgs-unstable = nixos-unstable.legacyPackages.${prev.system};
in
with final; {
  allcolors = with final; rustPlatform.buildRustPackage rec {
    pname = "allcolors";
    version = "0.1.0";
    src = fetchFromGitHub {
      owner = "polygon";
      repo = "allcolors-rs";
      rev = "023bd480245052357a7fd5f42181ff6e67d98b31";
      sha256 = "sha256-whaV+k5xh01OQNOehwkEBUDpMWn47mvVihVwchBvWoE=";
    };
    cargoPatches = [ ./allcolors-cargo-update.patch ];
    cargoSha256 = "sha256-RbfACA4hcyemGkw9bqjpIk393SBgBM939I95+grVI0c=";
    nativeBuildInputs = [ copyDesktopItems ];
    buildInputs = [
      xorg.libX11
      xorg.libXcursor
      xorg.libXrandr
      xorg.libXi
      libGL
      mesa
    ];
    postFixup = ''
      patchelf --set-rpath ${lib.makeLibraryPath buildInputs} $out/bin/allcolors-rs
    '';
    desktopItems = [ (makeDesktopItem {
      name = "allcolors";
      desktopName = "Polygon's allcolors-rs";
      categories = [ "Game" ];
      exec = "allcolors-rs";
    }) ];
  };

  # HACK: referenced by sdrweb
  # TODO: remove with 22.11
  alsaUtils = final.alsa-utils;

  inherit (bevy-julia.packages.${system}) bevy_julia;
  inherit (bevy-mandelbrot.packages.${system}) bevy_mandelbrot;

  bmxd = callPackage ./bmxd.nix { };

  dump1090-influxdb = callPackage ./dump1090-influxdb { };

  dump1090_rs = callPackage ./dump1090_rs.nix { };

  chromium = prev.chromium.override {
    commandLineArgs = "--enable-features=VaapiVideoEncoder,VaapiVideoDecoder,CanvasOopRasterization --force-dark-mode";
  };

  # hydra flake
  hydra = prev.hydra.overrideAttrs (oldAttrs: {
    patches = oldAttrs.patches or [ ] ++ [
      # gitea webhook support
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/pull/1227/commits/750978a19232583e17620a1bd80435e957e7213a.patch";
        sha256 = "sha256-86Li0YUSVUdnw6lt6kZ56ohDRKPD13SZzukqPU1np8U=";
      })
      # fix github webhook from orgs
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/commit/4d664ecb0faaf51b21673f979b543ea4694c3f1b.patch";
        sha256 = "sha256-lF5Rnz8r9ptyMLhcg/XnjiNhOK1KcLA7hi01ye4KgmI=";
      })
    ];
  });

  # hydra in nixpkgs
  hydra_unstable = prev.hydra_unstable.overrideAttrs (oldAttrs: {
    patches = oldAttrs.patches or [ ] ++ [
      # gitea webhook support
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/pull/1227/commits/750978a19232583e17620a1bd80435e957e7213a.patch";
        sha256 = "sha256-86Li0YUSVUdnw6lt6kZ56ohDRKPD13SZzukqPU1np8U=";
      })
      # fix github webhook from orgs
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/commit/4d664ecb0faaf51b21673f979b543ea4694c3f1b.patch";
        sha256 = "sha256-lF5Rnz8r9ptyMLhcg/XnjiNhOK1KcLA7hi01ye4KgmI=";
      })
    ];
  });

  mlat-client = prev.python3Packages.callPackage ./mlat-client.nix { };

  nixVersions =
    prev.nixVersions // {
      stable = prev.nixVersions.stable.overrideAttrs (oldAttrs: {
        patches = oldAttrs.patches or [ ] ++ lib.optionals (lib.versionAtLeast prev.nixVersions.stable.version "2.9") [
          # use github.com archive URL instead of api.github.com
          (fetchpatch {
            url = "https://github.com/NixOS/nix/pull/6997.patch";
            sha256 = "sha256-JaQ6OY1RjxCYymkI/x0rmKH8XVXYs5psRwNJ4TPHPS0=";
          })
        ];
      });
    };

  inherit (nixos-unstable.legacyPackages.${prev.targetPlatform.system}) nomad_1_3;

  openssh = prev.openssh.overrideAttrs (oldAttrs: {
    # takes 30 minutes
    doCheck = false;
  });

  pile = prev.callPackage ./pile.nix { };

  pi-sensors = prev.callPackage ./pi-sensors { };

  plume = prev.callPackage ./plume { };

  readsb = prev.callPackage ./readsb.nix { };

  SimpleYggGen-CPP = prev.callPackage ./simpleygggen.nix { };

  tracer-game =
    let
      broken = true;
      reason = "haddock runs on affection for 10 hours and more";
    in
      if broken
      then throw "tracer-game: ${reason}"
      else tracer.packages.${system}.tracer-game;

  # HACK: referenced by hydra-module.nix but removed from nixos-unstable in 2022-09
  # TODO: remove with 22.11
  utillinux = final.util-linux;

  # vector-0.23 + mqtt-sink
  vector = pkgs-unstable.callPackage ./vector {};

  wander =
    if prev ? wander
    then builtins.trace "`wander` is now available on stable NixOS. Please remove from overlay!"
      prev.wander
    else pkgs-unstable.wander;
}
