{ stdenv, fetchgit, fetchpatch, }:

stdenv.mkDerivation {
  pname = "bmxd";
  version = "1.1-2"; # ${version}-${release}

  src = fetchgit {
    url = "https://gitlab.freifunk-dresden.de/firmware-developer/firmware.git";
    rev = "T_FIRMWARE_7.0.3";
    sha256 = "sha256-LPlsfZvi0Hs9h0IM8Xtiv8GkkVLWs8LL+XSewTT+XHA=";
  };

  buildPhase = ''
    cd feeds/common/bmxd/sources
  '';

  installFlags = [
    "SBINDIR=${placeholder "out"}/sbin"
  ];
}
