# Registry of C3D2 machines.

{
  dacbert = {
    serial = "3c271952";
    ip4 = "172.22.99.203";
  };

  riscbert.ip4 = "riscbert.c3d2.zentralwerk.org";

  dn42 = {
    ip4 = "172.22.99.253";
  };

  freifunk = {
    ip4 = "172.20.72.40";
    publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMFbxHGfBMBjjior1FNRub56O62K++HVnqUH67BeKD7d";
  };

  gitea.publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO8Q7kGF3Hh6HvmlSIgZOjgoIZRpyxKvMBTcPWHlecuh";

  glotzbert = {
    ether = "ec:a8:6b:fe:b4:cb";
    publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAnEWn/8CKIiCtehh6Ha3XUQqjODj0ygyo3aGAsFWgfG";
    wol = true;
    ip4 = "glotzbert.hq.c3d2.de";
  };

  grafana = {
    ip6 = "2a00:8180:2c00:282:4042:fbff:fe4b:2de8";
    publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPFB9fo01jzr2upEBEXiR7sSmeQoq9ll5Cf5/hjq5e4Y";
  };

  mucbot = {
    ip4 = "172.20.73.27";
    ip6 = "2a00:8180:2c00:282:28db:dff:fe6b:e89a";
  };

  matemat = {
    ip4 = "172.20.73.21";
    ip6 = "2a00:8180:2c00:282:f82b:1bff:fedc:8572";
    publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBa07c4NnU1TGX1SMNea9e1d4nMtc0OS4gJLmTA3g/fe";
  };

  mpd-index = { };

  nfs = { };

  nncp = {
    ip6 = "2a00:8180:2c00:223:dcec:9aff:fe6f:3f63";
    publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMQhxaeElmxO1UgaI/+qr+g13OFeY9qtJVxznNN+xs/e";
  };

  public-access-proxy = {
    ip4 = "172.20.73.45";
    ip6 = "2a00:8180:2c00:282:1024:5fff:febd:9be7";
  };

  pulsebert = {
    ether = "dc:a6:32:31:b6:32";
    publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFQCsDss9Gq3/eTKqpgEwXK+nhnuARS4/kHqF2+laGnp";
    ip4 = "172.22.99.208";
  };

  samba = { };

  scrape = {
    ip4 = "172.20.73.32";
    ip6 = "2a00:8180:2c00:282:e073:50ff:fef5:eb6e";
    publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEGxPgg6nswoij1fBzDPDu6h4+d458XL2+dBxAx9KVOh";
  };

  schalter.ip4 = "schalter.hq.c3d2.de";

  # Hack
  rpi-netboot.ip4 = "127.0.0.1";

  server9.ip6 = "server9.cluster.zentralwerk.org";
  server10.ip6 = "server10.cluster.zentralwerk.org";
}
